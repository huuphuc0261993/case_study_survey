package com.codegym.cms.service.impl;

import com.codegym.cms.model.SanPham;
import com.codegym.cms.repository.SanPhamRepository;
import com.codegym.cms.service.SanPhamService;
import org.springframework.data.repository.query.Param;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public class SanPhamServiceImpl implements SanPhamService {
    @Autowired
    private SanPhamRepository sanphamRepository;

    @Override
    public SanPham findById(Long id) {
        return sanphamRepository.findById(id).get();
    }

    @Override
    public Iterable<SanPham> findAllByIsDeletedEquals(int isDeleted) {
        return sanphamRepository.findAllByIsDeletedEquals(isDeleted);
    }

    @Override
    public void softDelete(@Param("deleted_at") LocalDate deleted_at, @Param("deleted_by") String deleted_by, @Param("id") Long id) {
        sanphamRepository.softDelete(deleted_at, deleted_by, id);
    }


    @Override
    public void create(@Param("TenSanPham") String TenSanPham, @Param("Gia") int Gia, @Param("SoLuong") int SoLuong, @Param("MoTa") String MoTa, @Param("Anh") String Anh, @Param("created_at") LocalDate created_at, @Param("created_by") String created_by) {
        sanphamRepository.create(TenSanPham, Gia, SoLuong, MoTa, Anh, created_at, created_by);
    }

    @Override
    public void edit(@Param("TenSanPham") String TenSanPham, @Param("Gia") int Gia, @Param("SoLuong") int SoLuong, @Param("MoTa") String MoTa, @Param("Anh") String Anh, @Param("updated_at") LocalDate updated_at, @Param("updated_by") String updated_by, @Param("id") Long id) {
        sanphamRepository.edit(TenSanPham, Gia, SoLuong, MoTa, Anh, updated_at, updated_by, id);
    }
}