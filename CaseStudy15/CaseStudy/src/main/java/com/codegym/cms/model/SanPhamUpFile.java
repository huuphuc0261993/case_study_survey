package com.codegym.cms.model;


import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class SanPhamUpFile extends SanPham {
    private CommonsMultipartFile[] fileDatas;

    public SanPhamUpFile() {
    }

    public CommonsMultipartFile[] getFileDatas() {
        return fileDatas;
    }

    public void setFileDatas(CommonsMultipartFile[] fileDatas) {
        this.fileDatas = fileDatas;
    }
}
