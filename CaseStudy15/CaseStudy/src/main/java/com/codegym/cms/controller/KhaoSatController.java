package com.codegym.cms.controller;

import com.codegym.cms.model.*;
import com.codegym.cms.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.time.LocalDate;
import java.util.*;

@CrossOrigin(origins = "*")
@RestController
public class KhaoSatController {

    @Autowired
    private CauHoiService cauhoiService;

    @Autowired
    private XeService xeService;

    @ModelAttribute("hangxes")
    public Iterable<String> hangXes() {
        return xeService.hangXe2();
    }

    @Autowired
    private DapAnService dapanService;

    @Autowired
    private TraLoiService traloiService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private UserDoingSurveyService userdoingsurveyService;

    @Autowired
    private TraLoiChiTietService traloichitietService;

    @Autowired
    private UserService userService;

    @Autowired
    private NgayService ngayService;
    @Autowired
    private ThanhPhoService thanhPhoService;
    @Autowired
    private HuyenService huyenService;
    @Autowired
    private PhuongService phuongService;

    @ModelAttribute("diem")
    public int diem(Principal principal) {
        int diem = 0;
        if (!principal.getName().equals("anonymousUser")) {
            Long idUser = userService.idUser(principal.getName());
            User user = userService.findById(idUser);
            diem = user.getTongdiem();
        }
        return diem;
    }

    @ModelAttribute("ngays")
    public Iterable<Ngay> ngays() {
        return ngayService.findAllByIsDeletedEquals(0);
    }

    @Autowired
    private ThangService thangService;

    @ModelAttribute("thangs")
    public Iterable<Thang> thangs() {
        return thangService.findAllByIsDeletedEquals(0);
    }


    @GetMapping(value = "/cauhoi/{surveyId}/1", produces = "application/json;charset=UTF-8")
    public ModelAndView CauHoi(@PathVariable("surveyId") Long surveyId, Principal principal, HttpSession session) {
        Long idUser = userService.idUser(principal.getName());
        UserDoingSurvey userDoingSurvey = userdoingsurveyService.showUserDoingSurvey(surveyId, idUser);
        if (userDoingSurvey == null) {
            userdoingsurveyService.create(idUser, surveyId, LocalDate.now(), principal.getName());

        }
        Long maxSite = cauhoiService.maxSite();
        return getSite(1L, surveyId, maxSite, session);
    }

    @PostMapping(value = "/cauhoi/{surveyId}/{site}", produces = "application/json;charset=UTF-8")
    public ModelAndView create(@ModelAttribute("traloichitiet") TraLoiChiTiet traloichitiet, @RequestParam String idDapan, @RequestParam String idCauhoi, @PathVariable("site") Long site, @PathVariable("surveyId") Long surveyId, HttpSession session, Principal principal) {
        List<String> cauhoi_ids = Arrays.asList(idCauhoi.split(","));
        List<String> dapan_ids = Arrays.asList(idDapan.split(","));
        List<String> values = Arrays.asList(traloichitiet.getValue().split(","));

        Long idUserDoingSurvey;
        Long idUser = userService.idUser(principal.getName());
        for (String cauhoi_id : cauhoi_ids) {
            idUserDoingSurvey = userdoingsurveyService.showIdUserDoingSurvey(Long.valueOf(cauhoi_id), idUser);
            traloiService.create(idUserDoingSurvey, Long.valueOf(cauhoi_id), LocalDate.now(), principal.getName());
        }

        List<String> siteSession = new ArrayList<>();
        Long idTraloi;
        for (int i = 0; i < dapan_ids.size(); i++) {
            Long cauhoi_id = dapanService.findById(Long.valueOf(dapan_ids.get(i))).getCauHoi().getId();
            idUserDoingSurvey = userdoingsurveyService.showIdUserDoingSurvey(cauhoi_id, idUser);

            idTraloi = traloiService.idTraloi(Long.valueOf(dapan_ids.get(i)), idUserDoingSurvey);
            traloichitietService.create(idTraloi, Long.valueOf(dapan_ids.get(i)), traloichitiet.getName(), traloichitiet.getLuachon(), values.get(i), traloichitiet.getInnerText(), LocalDate.now(), "Dan");

            List<Long> cauHoiSessions = cauhoiService.listIdCHSession();
            Long idCauHoiByDapAn = dapanService.findById(Long.valueOf(dapan_ids.get(i))).getCauHoi().getId();

            if (cauHoiSessions.contains(idCauHoiByDapAn)) {
                siteSession.add(values.get(i));
                session.setAttribute("cau" + idCauHoiByDapAn, siteSession);
            }

        }

        Long maxSite = cauhoiService.maxSite();


        if (site > maxSite) {
            User user = userService.findById(idUser);
            Survey survey = surveyService.findById(surveyId);
            userService.edit(user.getUsername(), survey.getDiem() + user.getTongdiem(), user.getPassword(), LocalDate.now(), "Dan", idUser);
            return new ModelAndView("/home/survey");
        } else
            return getSite(site, surveyId, maxSite, session);

    }

    private ModelAndView getSite(@PathVariable("site") Long site, @PathVariable("surveyId") Long surveyId, @PathVariable("maxSite") Long maxSite, HttpSession session) {
        Iterable<CauHoi> cauhois = cauhoiService.show1(surveyId, site);
        List<DapAn> dapAnss = new ArrayList<>();
        ModelAndView modelAndView = new ModelAndView("/khaosat/khaosat");

        for (CauHoi cauhoi : cauhois) {
            String[] loai = cauhoi.getLoai().getTenLoai().split(" ");
            if (loai.length > 1) {
                String cauHoiSession = loai[1];
                List<String> dapAnSession = (List<String>) session.getAttribute("cau" + cauHoiSession);

                List<Long> iDCHByLoais = cauhoiService.iDCHByLoai(cauhoi.getLoai().getId());
                if (iDCHByLoais.contains(cauhoi.getId())) {
                    int index = iDCHByLoais.indexOf(cauhoi.getId());

                    List<String> randomSeries;
                    if (index == 0) {
                        Collections.shuffle(dapAnSession);
                        int randomSeriesLength = iDCHByLoais.size();
                        randomSeries = dapAnSession.subList(0, randomSeriesLength);
                        session.setAttribute("randomSeries", randomSeries);

                        Iterable<String> dongXes = xeService.dongXe(randomSeries.get(0));
                        modelAndView.addObject("dongXes", dongXes);
                        modelAndView.addObject("cauHoiSession", cauHoiSession);
                    } else {
                        randomSeries = (List<String>) session.getAttribute("randomSeries");
                        Iterable<String> dongXes = xeService.dongXe(randomSeries.get(index));
                        modelAndView.addObject("dongXes", dongXes);
                        modelAndView.addObject("cauHoiSession", cauHoiSession);
                    }
                }
            }

            Iterable<DapAn> dapAns = cauhoiService.showda(cauhoi);
            for (DapAn d : dapAns) {
                dapAnss.add(d);
            }
        }

        if (dapAnss.size() > 0) {
            modelAndView.addObject("cauhois", cauhois);
            modelAndView.addObject("dapAnss", dapAnss);
            modelAndView.addObject("traloichitiet", new TraLoiChiTiet());
            modelAndView.addObject("surveyId", surveyId);
            modelAndView.addObject("site", site);
            modelAndView.addObject("maxSite", maxSite);
            return modelAndView;

        } else {
            return new ModelAndView("/error-404");
        }
    }

    @RequestMapping(value = "/hangxe/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<String>> listHangXe() {
        Iterable<String> hangXes = xeService.hangXe();
        if (hangXes == null) {
            return new ResponseEntity<Iterable<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<String>>(hangXes, HttpStatus.OK);
    }

    @RequestMapping(value = "/hangxe/{dongxe}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<Iterable<String>> listHangXe(@PathVariable("dongxe") String dongxe) {
        Iterable<String> dongXes = xeService.dongXe(dongxe);
        if (dongXes == null) {
            return new ResponseEntity<Iterable<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<Iterable<String>>(dongXes, HttpStatus.OK);
    }

    @RequestMapping(value = "/thanhpho/", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> listThanhPho() {
        List<String> thanhphos = thanhPhoService.thanhPho();
        if (thanhphos == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(thanhphos, HttpStatus.OK);
    }

    @RequestMapping(value = "/huyen/{thanhpho}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> listHuyen(@PathVariable("thanhpho") String thanhpho) {
        List<String> huyens = huyenService.huyen(thanhpho);
        if (huyens == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(huyens, HttpStatus.OK);
    }

    @RequestMapping(value = "/phuong/{huyen}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<String>> listPhuong(@PathVariable("huyen") String huyen) {
        List<String> phuongs = phuongService.phuong(huyen);
        if (phuongs == null) {
            return new ResponseEntity<List<String>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<String>>(phuongs, HttpStatus.OK);
    }
}
