package com.codegym.cms.controller;

import com.codegym.cms.model.SanPham;
import com.codegym.cms.model.User;
import com.codegym.cms.service.SanPhamService;
import com.codegym.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.time.LocalDate;

@Controller
public class SercurityController {
    @Autowired
    private UserService userService;

    @Autowired
    private SanPhamService sanphamService;

    @ModelAttribute("diem")
    public int diem() {
        int diem = 0;
        if (!getPrincipal().equals("anonymousUser")) {
            Long idUser = userService.idUser(getPrincipal());
            User user = userService.findById(idUser);
            diem = user.getTongdiem();
        }
        return diem;
    }

    private String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

//        System.out.println(principal.);

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
//        System.out.println("userName  " + userName);
        return userName;
    }

    @GetMapping(value = {"/welcome"})
    public String fdsfsd(Model model) {
        model.addAttribute("user", getPrincipal());
        return "welcome";
    }

    @GetMapping(value = {"/home"})
    public String Homepage(Model model) {
        model.addAttribute("user", getPrincipal());
        return "/home/home";
    }

    //    @RequestMapping(value = "/admin", method = RequestMethod.GET)
//    public String adminPage(ModelMap model) {
//        model.addAttribute("user", getPrincipal());
//        return "admin";
//    }
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

//    @RequestMapping(value = "/dba", method = RequestMethod.GET)
//    public String dbaPage(ModelMap model) {
//        model.addAttribute("user", getPrincipal());
//        return "dba";
//    }

//    @RequestMapping(value="/login", method=RequestMethod.GET)
//    public String login() {
//        return "login";
//    }

    @GetMapping(value = "/about", produces = "application/json;charset=UTF-8")
    public ModelAndView about() {
        ModelAndView modelAndView = new ModelAndView("/home/about");
        return modelAndView;
    }

    @GetMapping(value = "/blog", produces = "application/json;charset=UTF-8")
    public ModelAndView blog() {
        ModelAndView modelAndView = new ModelAndView("/home/blog");
        return modelAndView;
    }

    @GetMapping(value = "/survey", produces = "application/json;charset=UTF-8")
    public ModelAndView survey() {
        ModelAndView modelAndView = new ModelAndView("/home/survey");
        return modelAndView;
    }

    @GetMapping(value = "/contact", produces = "application/json;charset=UTF-8")
    public ModelAndView contact() {
        ModelAndView modelAndView = new ModelAndView("/home/contact");
        return modelAndView;
    }

    @GetMapping(value = "/shop", produces = "application/json;charset=UTF-8")
    public ModelAndView shop() {
        Iterable<SanPham> sanphams = sanphamService.findAllByIsDeletedEquals(0);
        ModelAndView modelAndView = new ModelAndView("/home/shop");
        modelAndView.addObject("sanphams", sanphams);
        return modelAndView;
    }

    @PostMapping(value = "/shop", produces = "application/json;charset=UTF-8")
    public ModelAndView shoped(@RequestParam("id") Long id, @RequestParam("qty") int qty, Principal principal) {
        Long idUser = userService.idUser(principal.getName());
        User user = userService.findById(idUser);
        SanPham sanpham = sanphamService.findById(id);
        int gia = sanpham.getGia();

        sanphamService.edit(sanpham.getTenSanPham(), sanpham.getGia(), sanpham.getSoLuong() - qty, sanpham.getMoTa(), sanpham.getAnh(), LocalDate.now(), "Dan", sanpham.getId());
        userService.edit(user.getUsername(), user.getTongdiem() - (gia * qty), user.getPassword(), LocalDate.now(), "Dan", idUser);
        return shop();
    }

    @GetMapping(value = "/shopdetail/{id}", produces = "application/json;charset=UTF-8")
    public ModelAndView shopdetail(@PathVariable("id") Long id, Principal principal) {
        Long idUser = userService.idUser(principal.getName());
        User user = userService.findById(idUser);
        int diem = user.getTongdiem();
        SanPham sanpham = sanphamService.findById(id);

        ModelAndView modelAndView = new ModelAndView("/home/shopdetail");
        modelAndView.addObject("sanpham", sanpham);
        modelAndView.addObject("diem", diem);
        return modelAndView;
    }
}