package com.codegym.cms.controller;

import com.codegym.cms.model.SanPham;
import com.codegym.cms.service.SanPhamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

import com.codegym.cms.model.SanPhamUpFile;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Controller
public class SanPhamController {

    @Autowired
    private SanPhamService sanphamService;

    @GetMapping("/sanphams")
    public ModelAndView listSanPhams(@RequestParam(value = "s", required = false) String s) {
        Iterable<SanPham> sanphams;
        sanphams = sanphamService.findAllByIsDeletedEquals(0);

        ModelAndView modelAndView = new ModelAndView("/sanpham/list");
        modelAndView.addObject("sanphams", sanphams);
        return modelAndView;
    }


    @GetMapping("/create-sanpham")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/sanpham/create");
        modelAndView.addObject("sanphamUpFile", new SanPhamUpFile());
        return modelAndView;
    }

    @PostMapping("/create-sanpham")
    public ModelAndView checkValidation(@Valid @ModelAttribute("sanpham") SanPham sanpham, BindingResult bindingResult, HttpServletRequest request, @ModelAttribute("sanphamUpFile") SanPhamUpFile sanphamUpFile) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/sanpham/create");
            return modelAndView;
        } else {
            // Thư mục gốc upload file.
            String uploadRootPath = request.getServletContext().getRealPath("upload");
            //        System.out.println("uploadRootPath=" + uploadRootPath);
            File uploadRootDir = new File(uploadRootPath);
            // Tạo thư mục gốc upload nếu nó không tồn tại.
            if (!uploadRootDir.exists()) {
                uploadRootDir.mkdirs();
            }

            CommonsMultipartFile[] fileDatas = sanphamUpFile.getFileDatas();
            for (CommonsMultipartFile fileData : fileDatas) {
                // Tên file gốc tại Client.
                String name = fileData.getOriginalFilename();
//            System.out.println("Client File Name = " + name);
                if (name != null && name.length() > 0) {
                    try {
                        // Tạo file tại Server.
                        File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                        // Luồng ghi dữ liệu vào file trên Server.
                        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                        stream.write(fileData.getBytes());
                        stream.close();
                        sanpham.setAnh(uploadRootDir.getName() + File.separator + name);
                    } catch (Exception e) {
                        System.out.println("Error Write file: " + name);
                    }
                }
                System.out.println(sanpham.getTenSanPham());
                System.out.println(sanpham.getGia());
                sanphamService.create(sanpham.getTenSanPham(), sanpham.getGia(), sanpham.getSoLuong(), sanpham.getMoTa(), sanpham.getAnh(), LocalDate.now(), "Dan");
            }

            ModelAndView modelAndView = new ModelAndView("/sanpham/create");
            modelAndView.addObject("sanpham", new SanPham());
            modelAndView.addObject("message", "New sanpham created successfully");
            return modelAndView;
        }
    }

    // Phương thức này được gọi mỗi lần có Submit.
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
//        System.out.println("Target=" + target);

        if (target.getClass() == SanPhamUpFile.class) {
            // Đăng ký để chuyển đổi giữa các đối tượng multipart thành byte[]
            dataBinder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        }
    }

    @GetMapping("/edit-sanpham/{id}")
    public ModelAndView showEditForm(@PathVariable Long id) {
        SanPham sanpham = sanphamService.findById(id);
        if (sanpham != null) {
            SanPhamUpFile sanphamUpFile = new SanPhamUpFile();
            sanphamUpFile.setId(sanpham.getId());
            sanphamUpFile.setTenSanPham(sanpham.getTenSanPham());
            sanphamUpFile.setMoTa(sanpham.getMoTa());
            sanphamUpFile.setAnh(sanpham.getAnh());
            ModelAndView modelAndView = new ModelAndView("/sanpham/edit");
            modelAndView.addObject("sanphamUpFile", sanphamUpFile);
            return modelAndView;

        } else {
            ModelAndView modelAndView = new ModelAndView("/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/edit-sanpham")
    public ModelAndView updateSanPham(HttpServletRequest request, @ModelAttribute("sanphamUpFile") SanPhamUpFile sanphamUpFile) {
        // Thư mục gốc upload file.
        String uploadRootPath = request.getServletContext().getRealPath("upload");
        //        System.out.println("uploadRootPath=" + uploadRootPath);
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }

        CommonsMultipartFile[] fileDatas = sanphamUpFile.getFileDatas();
        for (CommonsMultipartFile fileData : fileDatas) {
            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();
//            System.out.println("Client File Name = " + name);
            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);
                    // Luồng ghi dữ liệu vào file trên Server.
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    sanphamUpFile.setAnh(uploadRootDir.getName() + File.separator + name);
                } catch (Exception e) {
                    System.out.println("Error Write file: " + name);
                }
            }
            sanphamService.edit(sanphamUpFile.getTenSanPham(), sanphamUpFile.getGia(), sanphamUpFile.getSoLuong(), sanphamUpFile.getMoTa(), sanphamUpFile.getAnh(), LocalDate.now(), "Dan", sanphamUpFile.getId());
        }
        ModelAndView modelAndView = new ModelAndView("/sanpham/edit");
        modelAndView.addObject("sanphamUpFile", sanphamUpFile);
        modelAndView.addObject("message", "SanPham updated successfully");
        return modelAndView;
    }

    @GetMapping("/delete-sanpham/{id}")
    public String showDeleteForm(@PathVariable Long id) {
        sanphamService.softDelete(LocalDate.now(), "Dan3", id);
        return "redirect:/sanphams";
    }

    @GetMapping("/view-sanpham/{id}")
    public ModelAndView viewSanPham(@PathVariable("id") Long id) {
        SanPham sanpham = sanphamService.findById(id);
        if (sanpham == null) {
            return new ModelAndView("/error.404");
        }

        ModelAndView modelAndView = new ModelAndView("/sanpham/view");
        modelAndView.addObject("sanpham", sanpham);
        return modelAndView;
    }
}